package ir.syborg.app.HangMan;

import java.util.ArrayList;
import android.app.Activity;
import android.graphics.Color;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.TextView;


public class ActivityHangMan extends Activity {

    public String[]          wordList      = new String[]{
                                           "محمد",
                                           "النا",
                                           "افسانه",
                                           "امیر",
                                           "برنامه نویسی",
                                           "بنفشه",
                                           "گل رز",
                                           "زرافه",
                                           "طوطی",
                                           "صندلی",
                                           "افتابه",
                                           "کامپیوتر",
                                           "ساعت",
                                           "رز",
                                           "مریم",
                                           "حسن",
                                           };
    public String            CurentWord;
    public TextView          txtSpace;
    public TextView          txtWrong;
    public int               numberWrong;
    public int               numberTrue;
    public ArrayList<Button> invisibleList = new ArrayList<Button>();


    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.main);

        Button btnNewGame = (Button) findViewById(R.id.btnNewGame);
        txtSpace = (TextView) findViewById(R.id.txtSpace);
        txtWrong = (TextView) findViewById(R.id.txtWrong);
        btnNewGame.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View arg0) {
                NewGame();

            }
        });
        NewGame();
    }


    public void NewGame() {
        int RandomNumber = (int) (Math.random() * wordList.length);
        CurentWord = wordList[RandomNumber];
        String Space = "";
        for (int i = 0; i < CurentWord.length(); i++) {
            Space = Space + "-";
        }
        txtSpace.setText(Space);
        txtWrong.setText(Space);

        for (Button btnList: invisibleList) {
            btnList.setVisibility(View.VISIBLE);

        }
        invisibleList.clear();
        numberTrue = 0;
        numberWrong = 0;
        txtSpace.setTextColor(Color.parseColor("#ffffff"));

    }


    public void InputKeyWord(View view) {
        Button btnInput = (Button) view;
        btnInput.setVisibility(View.INVISIBLE);
        invisibleList.add(btnInput);
        char strInput = btnInput.getText().toString().toCharArray()[0];

        if (CurentWord.contains("" + strInput)) {
            char[] spaceList = txtSpace.getText().toString().toCharArray();
            char[] wordLists = CurentWord.toCharArray();
            for (int i = 0; i < wordLists.length; i++) {
                if (wordLists[i] == strInput) {
                    spaceList[i] = strInput;
                    numberTrue++;
                }
            }
            txtSpace.setText(new String(spaceList));
            if (numberTrue == CurentWord.length()) {
                txtSpace.setText("تبریک! شما برنده شدید.");
                txtSpace.setTextColor(Color.parseColor("#11ff11"));

                return;
            }

        } else {
            if (numberWrong > CurentWord.length() - 1) {
                txtSpace.setText("متاسفانه شما باختید!!!");
                txtSpace.setTextColor(Color.parseColor("#ff1111"));

                return;

            }
            String strWrong = txtWrong.getText().toString();
            char[] charList = strWrong.toCharArray();
            charList[numberWrong] = strInput;
            txtWrong.setText(new String(charList));
            numberWrong++;

        }
    }
}